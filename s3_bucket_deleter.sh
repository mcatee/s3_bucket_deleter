#!/bin/bash

# Get the current date and time
now=$(date)

# Iterate through each line in the bucketList file
while read bucketName; do
  # Empty the bucket
  aws s3 rm s3://$bucketName --recursive

  # Delete the bucket and capture the output and exit code
  output=$(aws s3 rb s3://$bucketName 2>&1)
  exitCode=$?

  if [ $exitCode -eq 0 ]; then
    # Append the current date and time and the bucket name to the history file
    echo "$now: $bucketName" >> history
  else
    # Append the current date and time, the bucket name, and the error message to the errors file
    echo "$now: $bucketName: $output" >> errors
  fi
done < bucketList

# Clear the contents of the bucketList file
> bucketList
