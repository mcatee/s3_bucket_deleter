# s3_bucket_deleter

## Requirements

- AWS CLI with the API keys set with the appropriate permissions
- A file named `bucketList` with each line containing an S3 bucket to be emptied and deleted.
   - This file should be located in the working directory
- Ensure the bash script is executable (`chmod +x ./s3_bucket_deleter.sh`)

## Usage

`./s3_bucket_deleter.sh`

## License

Public domain (CC0 if conventional public domain licensing does not apply in your locality)
